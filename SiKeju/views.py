from django.shortcuts import render
from django.http import JsonResponse, HttpResponse, HttpResponseRedirect
from django.urls import reverse
from django.views.decorators.http import require_http_methods
from django.views.decorators.csrf import csrf_exempt
from django.core.exceptions import SuspiciousOperation
from django.db import IntegrityError
from .model_helper import *
import json

# Create your views here.
response = {}

def index(request):
    reserve = getAllReserve()
    response['reserve'] = reserve
    html = 'SiKeju/awal.html'
    return render(request, html, response)

@csrf_exempt
@require_http_methods(['GET','POST'])
def tambahJadwal(request):
    print("A")
    print(request.method)
    if request.method == 'GET' :
        return render(request,'SiKeju/indexPlus.html',response)
    else :
        print("c")

@csrf_exempt
@require_http_methods(['GET','POST'])
def put(request):
    try:
        data = json.loads(request.body.decode('utf-8'))
        hariCari = data['hari']
        jamCari = int(data['jam'])
        idRuang = data['ruang']
        idHari = 0
        idJam = 0
        ida = 0
        counter = 0
        print (data)
        done = False
        
        while(counter != 15) :
            counter += 1
            if hariCari == 'Senin':
                idHari = 0
            elif hariCari == 'Selasa':
                idHari = 1
            elif hariCari == 'Rabu': 
                idHari = 2
            elif hariCari == 'Kamis':
                idHari = 3
            elif hariCari == 'Jumat':
                idHari = 4
                
            if jamCari == 9:
                idJam = 1
            elif jamCari == 13:
                idJam = 2
            elif jamCari == 16:
                idJam = 3
            try:
                ida = (3*idHari) +  idJam;
                # print (ida)
                if int(data['jumlah_kelas']) <= (10 - countReserve(ida)):
                    j = 0
                    cek = 0
                    while j < int(data['jumlah_kelas']):
                        cek += 1
                        ruang = getRuang(idRuang)
                        print (ruang)
                        if int(data['kapasitas']) < ruang["kapasitas"] and not getReserve(ida, idRuang):
                            putReserve(ida, idRuang, data['mata_kuliah'])
                            j = j+1
                            if j == int(data['jumlah_kelas']) :
                                done = True
                        else :
                            if idRuang == "R01":
                                idRuang = "R02"
                            elif idRuang == "R02":
                                idRuang = "R03"
                            elif idRuang == "R03":
                                idRuang = "R04"
                            elif idRuang == "R04":
                                idRuang = "R05"
                            elif idRuang == "R05":
                                idRuang = "R06"
                            elif idRuang == "R06":
                                idRuang = "R07"
                            elif idRuang == "R07":
                                idRuang = "R08"
                            elif idRuang == "R08":
                                idRuang = "R09"
                            elif idRuang == "R09":
                                idRuang = "R10"
                            elif idRuang == "R10":
                                idRuang = "R01"
                        if cek == 10:
                            break
                else:
                    if jamCari == 9:
                        jamCari = 13
                    elif jamCari == 13:
                        jamCari = 16
                    else :
                        if hariCari == 'Senin':
                            hariCari = 'Selasa'
                        elif hariCari == 'Selasa':
                            hariCari = 'Rabu'
                        elif hariCari == 'Rabu':
                            hariCari = 'Kamis'
                        elif hariCari == 'Kamis':
                            hariCari = 'Jumat'
                        else :
                            hariCari = 'Senin'
                        jamCari = 9
            except Exception as e:
                print (e)
                
            if done :
                break

            if counter == 15:
                return JsonResponse({'status':'invalid'})
        return JsonResponse({'status':'valid'})
    except IntegrityError:
        return JsonResponse({'status':'invalid'})
    except:
        return HttpResponse("<h1>Bad Request</h1>",status=400)