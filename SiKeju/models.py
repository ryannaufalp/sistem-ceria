from django.db import models

# Create your models here.
class Jadwal(models.Model):
	no_slot = models.IntegerField(primary_key=True, unique=True)
	hari = models.CharField(max_length=100)
	jam = models.IntegerField()
	
class Ruang(models.Model):
	nama_ruang = models.CharField(max_length=100, primary_key = True, unique=True)
	kapasitas = models.IntegerField(max_length = 10)

class Reserve(models.Model):
	jadwal = models.ForeignKey(Jadwal, on_delete=models.CASCADE)
	ruang = models.ForeignKey(Ruang, on_delete=models.CASCADE)
	matkul = models.CharField(max_length=100)

	class Meta:
		unique_together = (("jadwal", "ruang"),)