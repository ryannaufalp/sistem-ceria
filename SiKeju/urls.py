from django.conf.urls import url
from .views import *

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^tambahJadwal/', tambahJadwal, name='tambahJadwal'),
    url(r'^put/', put, name='put')
]