import sqlite3
from .models import *

#Ngambil jadwal dengan parameter no_slot (1-15)
#Return value tuple (no slot, hari, jam). Contoh (1, 'Senin', 9)
#Kalau ga ada return None
def getJadwal(no_slot):
	return Jadwal.objects.get(no_slot=no_slot)

#Ngambil ruangan dengan parameter nama_ruang (R1-R10)
#Return value tuple (nama ruang, kapasitas). Contoh (R1, 60)
#Kalau ga ada return None
def getRuang(nama_ruang):

	try:
		r = Ruang.objects.get(nama_ruang=nama_ruang)
	except Exception as e:
		print(e)
	return {"kapasitas" : r.kapasitas, "nama_ruang" : r.nama_ruang}

#Ngambil reserve dengan parameter nama_ruang (R1-R10) dan no_slot(1-15)
#Return value tuple (no_slot, nama_ruang). Contoh (1, R3)
#Kalau ga ada return None
def getReserve(no_slot, nama_ruang):
	return Reserve.objects.filter(jadwal_id=no_slot, ruang_id=nama_ruang).exists()
#Insert data ke tabel
#Kalo belom ada di reserve masukin, return true
#Kalo udah ada di reserve return false
def putReserve(no_slot, nama_ruang, matkul):
	try:

		j = Jadwal.objects.get(no_slot=no_slot)

		r = Ruang.objects.get(nama_ruang=nama_ruang)

		Reserve.objects.create(jadwal=j, ruang=r, matkul=matkul)

	except Exception as e:
		print (e)
	return True

#Parameter no_slot (1-15)
#Return jumlah kelas yang sudah terpakai di suatu slot
def countReserve(no_slot):
	res = 0
	try:
		for i in Reserve.objects.all():
			if i.jadwal.no_slot == no_slot:
				res+=1
	except Exception as e:
		print (e)
	return res

#Return array isinya tuple reserve. Contoh [(1,"R01"), (1,"R10")]
def getAllReserve():
	arr = []
	for i in Reserve.objects.all():
		arr.append({
			"hari" : i.jadwal.hari,
			"ruang" : i.ruang.nama_ruang,
			"jam" : i.jadwal.jam,
			"matkul" : i.matkul
			})
	return arr
