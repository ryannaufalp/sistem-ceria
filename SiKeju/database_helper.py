import sqlite3

#Ngambil jadwal dengan parameter no_slot (1-15)
#Return value tuple (no slot, hari, jam). Contoh (1, 'Senin', 9)
#Kalau ga ada return None
def getJadwal(no_slot):
	con = sqlite3.connect("siscer.db")
	c = con.cursor()

	c.execute("select * from jadwal where no_slot=?", (no_slot,))
	res = c.fetchone()
	
	c.close()
	con.close()
	return res

#Ngambil ruangan dengan parameter nama_ruang (R1-R10)
#Return value tuple (nama ruang, kapasitas). Contoh (R1, 60)
#Kalau ga ada return None
def getRuang(nama_ruang):
	con = sqlite3.connect("siscer.db")
	c = con.cursor()

	c.execute("select * from ruang where no_slot=?", (nama_ruang,))
	res = c.fetchone()
	
	c.close()
	con.close()
	return res

#Ngambil reserve dengan parameter nama_ruang (R1-R10) dan no_slot(1-15)
#Return value tuple (no_slot, nama_ruang). Contoh (1, R3)
#Kalau ga ada return None
def getReserve(no_slot, nama_ruang):
	con = sqlite3.connect("siscer.db")
	c = con.cursor()
	
	c.execute("select * from reserve where no_slot=? and nama_ruang=?", (no_slot, nama_ruang,))
	res = c.fetchone()
	
	c.close()
	con.close()
	return res

#Insert data ke tabel
#Kalo belom ada di reserve masukin, return true
#Kalo udah ada di reserve return false
def putReserve(no_slot, nama_ruang, matkul):

	if getReserve(no_slot, nama_ruang):
		return False
	else:
		con = sqlite3.connect("siscer.db")
		c = con.cursor()

		c.execute("pragma foreign_keys = on;")
		c.execute("insert into reserve values (?,?,?)", (no_slot, nama_ruang, matkul,))
		c.execute("pragma foreign_keys = off")

		con.commit()
		c.close()
		con.close();
		return True

#Parameter no_slot (1-15)
#Return jumlah kelas yang sudah terpakai di suatu slot
def countReserve(no_slot):
	con = sqlite3.connect("siscer.db")
	c = con.cursor()
	
	i = 0
	for row in c.execute("select * from reserve where no_slot=?", (no_slot,)):
		i+=1

	c.close()
	con.close()
	return i

#Return array isinya tuple reserve. Contoh [(1,"R01"), (1,"R10")]
def getAllReserve():
	con = sqlite3.connect("siscer.db")
	c = con.cursor()
	
	arr = []
	for row in c.execute("select * from reserve order by no_slot asc, nama_ruang asc"):
		arr.append(row)

	c.close()
	con.close()
	return arr
