var vm = new Vue({
    delimiters: ['$(', ')'],
    el: '#app',
    data: {
        title: "Masukkan Jadwal Ujian yang diinginkan!",
        alert_success:false,
        alert_danger:false,
        mata_kuliah : { value: "", valid: true },
        hari: { value: "", valid: true },
		jam: { value: "", valid: true },
		ruang: { value: "", valid: true },
		kapasitas: { value: "", valid: true },
		jumlah_kelas: { value: "", valid: true } 
    },
    methods: {
        submitForm: function (e) {
            e.preventDefault();
            var postData = {}
            var check = this.mata_kuliah.valid && this.hari.valid && this.jam.valid && this.ruang.valid && this.kapasitas.valid && this.jumlah_kelas.valid;
            if(check){
                this.$http.post('/tambahJadwal/',JSON.stringify(this.post_data))
                .then(res => res.json())
                .then(res => {
                    console.log(res.status);
                    if(res.status == "valid"){
                        this.alert_danger=false;
                        this.alert_success=true;
                        setTimeout(function() {
                            location.href="/tambahJadwal";
                        },3000);
                    } else{
                        this.alert_success=false;
                        this.alert_danger=true;
                    }
                })
                .catch(err => console.log(err));
            } else {
                alert('Form is Invalid');
            }
        },
        cancelRegister: function() {
            location.href="/index";
        },
    },
    computed: {
        post_data: function() {
            var form_data = {}
            form_data["mata_kuliah"] = this.mata_kuliah.value;
			form_data["hari"] = this.hari.value;
			form_data["jam"] = this.jam.value;
			form_data["ruang"] = this.ruang.value;
			form_data["kapasitas"] = this.kapasitas.value;
			form_data["jumlah_kelas"] = this,jumlah_kelas.value;
            return form_data;
        }
    },
    watch: {
        mata_kuliah: {
            handler: function (oldVal, newVal) {
                if (newVal.value.length > 0 ) {
                    newVal.valid = true;
                } else {
                    newVal.valid = false;
                }
            },
            deep: true
        },
        hari: {
            handler: function (oldVal, newVal) {
                if (newVal.value.length > 0 ) {
                    newVal.valid = true;
                } else {
                    newVal.valid = false;
                }
            },
            deep: true
        },
		jam: {
            handler: function (oldVal, newVal) {
                if (newVal.value.length > 0 ) {
                    newVal.valid = true;
                } else {
                    newVal.valid = false;
                }
            },
            deep: true
        },
		ruang: {
            handler: function (oldVal, newVal) {
                if (newVal.value.length > 0 ) {
                    newVal.valid = true;
                } else {
                    newVal.valid = false;
                }
            },
            deep: true
        },
		kapasitas: {
            handler: function (oldVal, newVal) {
                if (newVal.value.length > 0 ) {
                    newVal.valid = true;
                } else {
                    newVal.valid = false;
                }
            },
            deep: true
        },
		jumlah_kelas: {
            handler: function (oldVal, newVal) {
                if (newVal.value.length > 0 ) {
                    newVal.valid = true;
                } else {
                    newVal.valid = false;
                }
            },
            deep: true
        },
    },
});
